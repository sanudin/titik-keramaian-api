const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
require("dotenv").config();

//controller
const coord = require("./controllers/coordinate");

//middleware
const app = express();
app.use(bodyParser.json());
app.use(cors());

//rest
app.get("/", (req, res) => {
  res.send("Welcome to Titik Keramaian API");
});
app.post("/answertest", (req, res) => {
  coord.answerTest(req, res);
});
app.post("/coords", (req, res) => {
  coord.coords(req, res);
});

//listen
app.listen(process.env.PORT || 3000, () => {
  console.log(`app is running on port ${process.env.PORT || 3000}`);
});
