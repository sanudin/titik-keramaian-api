const knex = require("knex");
const db = knex({
  client: "pg",
  connection: {
    connectionString: process.env.PG_CONN_STRING,
  },
});

const getCoordsAndTotalUser = (from, to) => {
  const query = `SELECT
                  longitude,
                  latitude,
                  SUM(user_count) AS total_user,
                  CONCAT('${from}', ' to ', '${to}') AS range
                FROM titik_keramaian
                WHERE time BETWEEN '${from}' AND '${to}'
                GROUP BY longitude, latitude
                ORDER BY longitude
                `;

  return new Promise((resolve, reject) => {
    db.raw(query)
      .then((data) => {
        if (data.rowCount <= 0) throw "No Data";

        resolve(data.rows);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

const getUserPerBrand = (from, to) => {
  const query = `SELECT 
                  longitude, 
                  latitude, 
                  brand, 
                  CONCAT('${from}', ' to ', '${to}') AS range,
                  SUM(user_count) AS user_per_brand
                FROM titik_keramaian
                WHERE time BETWEEN '${from}' AND '${to}'
                GROUP BY longitude, latitude, brand
                ORDER BY longitude, latitude, brand ASC
                `;

  return new Promise((resolve, reject) => {
    db.raw(query)
      .then((data) => {
        if (data.rowCount <= 0) return;

        resolve(data.rows);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

const getAnswerTest = (from, to) => {
  const query = `SELECT 
                tk.longitude,
                tk.latitude,
                tk.brand, 
                CONCAT('2021-10-20 07:00:00', ' to ', '2021-10-20 08:00:00') AS range,
                SUM(tk.user_count) AS user_per_brand, 
                ts.total_user
              FROM titik_keramaian tk
              LEFT JOIN (
                SELECT longitude, latitude, SUM(user_count) AS total_user
                FROM titik_keramaian 
                GROUP BY longitude, latitude
              ) AS ts
              ON tk.longitude=ts.longitude AND tk.latitude=ts.latitude
              WHERE 
                tk.time BETWEEN '2021-10-20 07:00:00' AND '2021-10-20 08:00:00'
              GROUP BY tk.longitude, tk.latitude, tk.brand, ts.total_user
              ORDER BY tk.brand ASC
                `;

  return new Promise((resolve, reject) => {
    db.raw(query)
      .then((data) => {
        if (data.rowCount <= 0) return;

        resolve(data.rows);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

module.exports = {
  getCoordsAndTotalUser,
  getUserPerBrand,
  getAnswerTest,
};
