const getData = require("../models/getdata");

const buildGeoJson = (totalUSer, brand) => {
  if (!totalUSer || !brand) return;

  let geoJson = {
    type: "FeatureCollection",
    features: [],
  };

  totalUSer.forEach((dataI, i) => {
    const lng = dataI.longitude;
    const lat = dataI.latitude;

    let feature = {
      type: "Feature",
      properties: {
        range: dataI.range,
        total_user: Number(dataI.total_user),
        data_brand: [],
      },
      geometry: {
        type: "Point",
        coordinates: [lng, lat],
      },
    };

    brand.forEach((dataJ, j) => {
      if (dataJ.longitude === lng && dataJ.latitude === lat) {
        const dataBrand = {
          brand: dataJ.brand,
          user_per_brand: dataJ.user_per_brand,
        };

        feature.properties.data_brand.push(dataBrand);
      }
    });

    geoJson.features.push(feature);
  });

  return geoJson;
};

// const latlng = (req, res) => {
//   db("titik_keramaian")
//     .select(db.raw("longitude, latitude, SUM(user_count) AS total_user"))
//     .groupBy("longitude", "latitude")
//     .then((data) => {
//       return res.json(data);
//     })
//     .catch((err) => res.status(400).json(`Can't retrieve data`));
// };

const answerTest = (req, res) => {
  getData
    .getCoordsAndTotalUser("2021-10-20 07:00:00", "2021-10-20 08:00:00")
    .then((dataTotal) => {
      getData
        .getAnswerTest()
        .then((data) => {
          const geoObject = buildGeoJson(dataTotal, data);

          return res.json(geoObject);
        })
        .catch((err) => {
          if (err === "No Data") res.status(400).json(err);
          res.status(400).json(`Can't retrieve data`);
        });
    })
    .catch((err) => {
      if (err === "No Data") res.status(400).json(err);
      res.status(400).json(`Can't retrieve data`);
    });
};

const coords = (req, res) => {
  const { from, to } = req.body;

  getData
    .getCoordsAndTotalUser(from, to)
    .then((dataTotal) => {
      getData
        .getUserPerBrand(from, to)
        .then((dataBrand) => {
          const geoObject = buildGeoJson(dataTotal, dataBrand);

          return res.json(geoObject);
        })
        .catch((err) => {
          if (err === "No Data") res.status(400).json(err);
          res.status(400).json(`Can't retrieve data`);
        });
    })
    .catch((err) => {
      if (err === "No Data") res.status(400).json(err);
      res.status(400).json(`Can't retrieve data`);
    });
};

module.exports = {
  answerTest,
  coords,
};
